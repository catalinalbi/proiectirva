﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FootScript : MonoBehaviour
{
    public AudioClip StepSound;
    private AudioSource audioSource;
    private bool canWalk = true;
    // Start is called before the first frame update
    void Start()
    {
        audioSource = GetComponent<AudioSource>();
        audioSource.clip = StepSound;
    }

    // Update is called once per frame
    void Update()
    {
    }

    private void OnCollisionEnter(Collision collision)
    {
        StartCoroutine(Step());
    }

    private IEnumerator Step()
    {
        if (canWalk)
        {
            canWalk = false;
            audioSource.Play();
            yield return new WaitForSeconds(0.38f);
            canWalk = true;
        }
    }
}
