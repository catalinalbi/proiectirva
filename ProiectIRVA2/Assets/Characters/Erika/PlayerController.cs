﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : PhysicsObject
{
    public GameObject ArrowSpawnPoint, Arrow, Camera;
    public Camera SecondCamera;
    public float Force = 5;
    public AudioClip Step1Sound, Step2Sound, JumpSound, BowSound, ShootSound;
    public AudioSource StepsAudioSource;

    private Animator animator;
    private CharacterController controller;
    private AudioSource audioSource;

    private bool run = false, aim = false, canShoot = true, jump = false, groundedPlayer, Step1 = true, Step2 = false;

    private Vector3 playerVelocity, move;

    private float playerSpeed = 1.95f;
    private float gravityValue = -9.81f;
    private float playerRunSpeed = 3.9f;

    // Start is called before the first frame update
    void Start()
    {
        this.animator = GetComponent<Animator>();
        this.controller = GetComponent<CharacterController>();
        this.audioSource = GetComponent<AudioSource>();
        audioSource.clip = BowSound;
        audioSource.clip.LoadAudioData();
    }

    void Update()
    {
        //Ray r = Camera.GetComponent<Camera>().ViewportPointToRay(new Vector3(0.5f + (float)70 / Screen.width, 0.5f + (float)100 / Screen.height, Camera.GetComponent<Camera>().nearClipPlane));
        //Debug.DrawRay(r.origin, r.direction * 1000, Color.cyan);
        move = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));
        run = false;
        if (Input.GetKey(KeyCode.LeftShift) && !aim)
        {
            run = true;
        }
        animator.SetBool("run", run);

        groundedPlayer = controller.isGrounded;
        if (groundedPlayer && playerVelocity.y < 0)
        {
            playerVelocity.y = 0f;
        }


        if (Input.GetKeyDown(KeyCode.Space) && !jump)
        {
            jump = true;
            audioSource.clip = JumpSound;
            audioSource.Play();
            if (move != Vector3.zero)
            {
                StartCoroutine(RunJump());
            }
            else
            {
                StartCoroutine(Jump());
            }
        }

        playerVelocity.y += gravityValue * Time.deltaTime;
        if (!animator.GetBool("shoot"))
        {
            controller.Move(transform.TransformDirection(move) * Time.deltaTime * (run ? playerRunSpeed : playerSpeed));
            controller.Move(playerVelocity * Time.deltaTime);
        }

        if (move.x != 0 && move.z != 0)
        {
            move.x = 0;
        }

        var rotationX = Input.GetAxis("Mouse X");
        var rotationY = Input.GetAxis("Mouse Y");
        transform.rotation = Quaternion.AngleAxis(transform.localEulerAngles.y + rotationX, Vector3.up);
        Camera.transform.localRotation =
            Quaternion.Slerp(Camera.transform.localRotation, Quaternion.Euler(Camera.transform.localEulerAngles.x - (rotationY * 2 * GameManager.Sensitivity), transform.localEulerAngles.y + (rotationX * GameManager.Sensitivity), 0), 20 * Time.deltaTime);
        var position = new Vector3(0, 0, 0);
        position.y += 2.8f;
        position.z -= 4;
        Camera.transform.position = Vector3.Lerp(Camera.transform.position, transform.TransformPoint(position), Time.deltaTime * 10);
        if (Input.GetMouseButton(1) && !jump && canShoot)
        {
            if (!audioSource.isPlaying && Camera.GetComponent<Camera>().fieldOfView > 55)
            {
                audioSource.clip = BowSound;
                audioSource.Play();
            }
            animator.SetBool("aim", true);
            aim = true;
            if (Camera.GetComponent<Camera>().fieldOfView >= 30)
            {
                Camera.GetComponent<Camera>().fieldOfView -= Time.deltaTime * 30 * 2;
            }
        }
        else
        {
            animator.SetBool("aim", false);
            aim = false;
            if (Camera.GetComponent<Camera>().fieldOfView <= 60)
            {
                Camera.GetComponent<Camera>().fieldOfView += Time.deltaTime * 30 * 2;
            }
        }
        if (!Input.GetMouseButton(1))
        {
            if (Camera.GetComponent<Camera>().fieldOfView <= 60)
            {
                Camera.GetComponent<Camera>().fieldOfView += Time.deltaTime * 30 * 2;
            }
        }

        if (Input.GetMouseButtonDown(0))
        {
            if (aim)
            {
                StartCoroutine(Shoot(1.5f));
            }
            else
            {
                StartCoroutine(Shoot(0.78f));
            }
        }

        if (move.x != 0 || move.z != 0)
        {
            //StartCoroutine(PlayStepsSound());
        }
        animator.SetFloat("walk_x", move.x);
        animator.SetFloat("walk_z", move.z);
    }

    private IEnumerator Attack()
    {
        animator.SetBool("attack", true);
        yield return new WaitForSeconds(2.317f);
        animator.SetBool("attack", false);
    }

    private IEnumerator Jump()
    {
        animator.SetBool("jump", jump);
        yield return new WaitForSeconds(1.1f);
        jump = false;
        animator.SetBool("jump", jump);
    }

    private IEnumerator RunJump()
    {
        animator.SetBool("jump", jump);
        yield return new WaitForSeconds(0.7f);
        jump = false;
        animator.SetBool("jump", jump);
    }

    private IEnumerator Shoot(float timeToWait)
    {
        if (canShoot)
        {
            canShoot = false;
            if (!aim)
            {
                animator.SetBool("shoot", true);
                yield return new WaitForSeconds(timeToWait / 2);
                audioSource.clip = ShootSound;
                audioSource.Play();
                yield return new WaitForSeconds(timeToWait / 2);
            }
            var currentArrow = Instantiate(Arrow, ArrowSpawnPoint.transform.position, Quaternion.Euler(0, transform.localEulerAngles.y, 0));
            Ray r = Camera.GetComponent<Camera>().ViewportPointToRay(new Vector3(0.5f + (float)70 / Screen.width, 0.5f + (float)100 / Screen.height, Camera.GetComponent<Camera>().nearClipPlane));
            var arrowHit = Physics.Raycast(r, out RaycastHit hit);

            var point = arrowHit ? hit.point : r.direction * (1000) + currentArrow.transform.position;
            var _direction = (point - currentArrow.transform.position).normalized;
            var _lookRotation = Quaternion.LookRotation(_direction);

            currentArrow.transform.rotation = Quaternion.Slerp(transform.rotation, _lookRotation, Time.deltaTime * 100);
            currentArrow.GetComponent<Rigidbody>().AddForce(_direction * Force, ForceMode.Impulse);
            if (aim)
            {
                audioSource.clip = ShootSound;
                audioSource.Play();
                yield return new WaitForSeconds(timeToWait);
            }
            if (!aim)
            {
                animator.SetBool("shoot", false);
            }
            canShoot = true;
        }
    }

    //private IEnumerator PlayStepsSound()
    //{
    //    //if (!StepsAudioSource.isPlaying && Step1)
    //    //{
    //    //    Step1 = false;
    //    //    StepsAudioSource.clip = Step1Sound;
    //    //    StepsAudioSource.Play();
    //    //    yield return new WaitForSeconds(0.45f);
    //    //    Step2 = true;
    //    //}
    //    //if (!StepsAudioSource.isPlaying && Step2)
    //    //{
    //    //    Step2 = false;
    //    //    StepsAudioSource.clip = Step2Sound;
    //    //    StepsAudioSource.Play();
    //    //    yield return new WaitForSeconds(0.45f);
    //    //    Step1 = true;
    //    //}
    //    //yield return new WaitForSeconds(0.01f);
    //}

}
