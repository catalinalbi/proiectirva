﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SecondCamera : MonoBehaviour
{
    public GameObject Hand;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        var rotationX = Input.GetAxis("Mouse X");
        var rotationY = Input.GetAxis("Mouse Y");
        transform.localRotation =
            Quaternion.Slerp(transform.localRotation, Quaternion.Euler(transform.localEulerAngles.x - (rotationY * 2 * GameManager.Sensitivity), transform.localEulerAngles.y + (rotationX * GameManager.Sensitivity), 0), 20 * Time.deltaTime);
        var position = new Vector3(0, 0, 0);
        position.y += 2.8f;
        position.z -= 4;
        transform.position = Vector3.Lerp(transform.position, Hand.transform.position, Time.deltaTime * 10);
    }
}
