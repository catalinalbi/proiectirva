﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Player : MonoBehaviour
{
    public GameObject HealthBar, HealthText;
    private int Health = 100;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    private void Restart()
    {
        Health = 100;
    }

    // Update is called once per frame
    void Update()
    {
    }

    public void TakeDMG(BodyPart bodyPart)
    {
        switch(bodyPart)
        {
            case (BodyPart.Foot):
                Health -= 5;
                break;
            case (BodyPart.Leg):
                Health -= 7;
                break;
            case (BodyPart.UpperLeg):
                Health -= 10;
                break;
            case (BodyPart.Torso):
                Health -= 13;
                break;
            case (BodyPart.Chest):
                Health -= 15;
                break;
            case (BodyPart.Neck):
                Health -= 20;
                break;
            case (BodyPart.Head):
                Health -= 30;
                break;
        }
        HealthText.GetComponent<Text>().text = Health.ToString();
        HealthBar.transform.localScale = new Vector3(Health / 100, HealthBar.transform.localScale.y, HealthBar.transform.localScale.z);
        if (Health <= 0)
        {
            PlayerDead();
        }
    }

    private void PlayerDead()
    {

    }

    public enum BodyPart
    {
        Foot = 0,
        Leg = 1,
        UpperLeg = 2,
        Torso = 3,
        Chest = 4,
        Neck = 5,
        Head = 6
    }
}
