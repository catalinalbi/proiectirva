﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class CameraController : MonoBehaviour
{
    public GameObject Player;
    public float sensitivity = 100;
    private Vector3 offset;
    private Vector3 oldOffset;
    // Start is called before the first frame update
    void Start()
    {
        offset = new Vector3(Player.transform.position.x, Player.transform.position.y + 2.8f, Player.transform.position.z - 4.0f);
    }

    // Update is called once per frame
    //void Update()
    //{
    //    if (Input.GetMouseButton(1))
    //    {
    //        offset = Quaternion.AngleAxis(Input.GetAxis("Mouse X") * Time.deltaTime * sensitivity, Vector3.up) * offset;
    //        offset = Quaternion.AngleAxis(-Input.GetAxis("Mouse Y") * Time.deltaTime * sensitivity, Vector3.right) * offset;
    //        var finalOffset = Player.transform.position + offset;

    //        if (finalOffset.y < 0.5f)
    //        {
    //            finalOffset.y = 0.6f;
    //            offset.y = 0.6f;
    //        }
    //        else if (finalOffset.y > 4.5f)
    //        {
    //            finalOffset.y = 4.4f;
    //            offset.y = 4.4f;
    //        }
    //        transform.position = Vector3.Lerp(transform.position, finalOffset, Time.deltaTime * 10);
    //        transform.LookAt(Player.transform.position);
    //    }
    //    else
    //    {
    //        var finalOffset = Player.transform.position + offset;
    //        if (finalOffset.y < 0.5f)
    //        {
    //            finalOffset.y = 0.6f;
    //            offset.y = 0.6f;
    //        }
    //        else if (finalOffset.y > 4.5f)
    //        {
    //            finalOffset.y = 4.4f;
    //            offset.y = 4.4f;
    //        }
    //        transform.position = Vector3.Lerp(transform.position, finalOffset, Time.deltaTime * 10);
    //    }
    //}

    private void Update()
    {
        var position = Player.transform.TransformDirection(new Vector3(1, 0, 0));
        position.y += 2.8f;
        position.z -= 4;
        //transform.position = Vector3.Lerp(transform.position, position, Time.deltaTime * 10);
        //transform.position = Player.transform.position + Player.transform.TransformDirection(new Vector3(1, 0, 0));
        //Debug.Log(Player.transform.localEulerAngles.y - transform.localEulerAngles.y);
        //transform.RotateAround(Player.transform.position, Vector3.up, Player.transform.localEulerAngles.y - transform.localEulerAngles.y);
    }
}
